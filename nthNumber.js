const nth = (arr, nth) => arr.filter((e, i) => i % nth === nth - 1);

console.log(nth([0,1,1,2,3,5,8,13,21],5))