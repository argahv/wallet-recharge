


export default (state = [], action) => {
    switch (action.type) {
        case 'ADD_TRANSACTION':
            return [
                ...state,
                action.wallet
            ]
        default: return state
    }

}
