import {combineReducers} from 'redux'
import walletReducer from '../reducers/transaction'


export default combineReducers({
    wallet:walletReducer,
})