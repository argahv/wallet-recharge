import React from 'react'

const TransactionHistoryListItem = ({ number, amount, balance }) =>
    (
        <div>
            <h1>Phone Number : {number}</h1>
            <p>Initial wallet Balance: {balance}</p>
            <p>Amount transferred: {amount}</p>
            <p>New wallet balance: {balance - amount}</p>
        </div>

    )

export default TransactionHistoryListItem