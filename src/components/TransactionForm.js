import React from 'react'

export default class TransactionForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            number: props.wallet ? props.wallet.number : '',
            amount: props.wallet ? props.wallet.amount : '',
            info: ''
        }
    }

    onNumberChange = e => {
        const number = e.target.value
        if (!number || number.match(/^\d{1,}(\.\d{0,2})?$/)) {
            this.setState(() => ({ number }));
        }
    }

    onAmountChange = e => {
        const amount = e.target.value
        if (!amount || amount.match(/^\d{1,}(\.\d{0,2})?$/)) {
            this.setState(() => ({ amount }));
        }
    }



    onSubmit = e => {
        e.preventDefault();
        

        const threestr = this.state.number.substring(0, 3)
       
        if (threestr === "986" || threestr === '984') {
            this.setState(() => ({
                info: 'NTC'
            }))
        } 
         if (threestr === '980' || threestr === '981') {
            this.setState(() => ({
                info: 'NCELL'
            }))
        }
        
            this.props.onSubmit({
                amount: this.state.amount,
                number: this.state.number

            })
        
    }
    render() {
        return (
            <div>
                <p>{this.state.info}</p>
                <form onSubmit={this.onSubmit}>
                    <input
                        type="tel"
                        placeholder="Enter the number"
                        autoFocus
                        value={this.state.number}
                        onChange={this.onNumberChange}
                    />
                    <input
                        type="text"
                        placeholder="amount"
                        value={this.state.amount}
                        onChange={this.onAmountChange}
                    />
                    <button>Transfer</button>
                </form>

            </div>

        )
    }
}
