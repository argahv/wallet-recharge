import React from 'react'
import { connect } from 'react-redux'
import TransactionHistoryListItem from './TransactionHistoryListItem'

const TransactionHistoryList = (props) => {
    return (
        <div>
            <h1>History:</h1>
            {props.wallet.map((wallet) => {
                return <TransactionHistoryListItem
                    key={wallet.number}
                    {...wallet} />
            })}
        </div>
    )
}


const mapStateToProps = state => ({
    wallet: state.wallet
})

export default connect(mapStateToProps)(TransactionHistoryList)