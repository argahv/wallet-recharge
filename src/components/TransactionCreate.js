import React from 'react'
import { addTransaction } from '../actions'
import { connect } from 'react-redux'
import TransactionForm from './TransactionForm'

const TransactionCreate = (props) => {
    return (
        <div>
            <h3>Create a Transaction</h3>
            <TransactionForm
                onSubmit={
                    (wallet) => {
                        props.dispatch(addTransaction(wallet));

                    }} />
        </div>
    )
}



export default connect()(TransactionCreate)