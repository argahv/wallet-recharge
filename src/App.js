import React from 'react'
import TransactionCreate from './components/TransactionCreate'
import TransactionHistoryList from './components/TransactionHistoryList';

const App =()=>(
    <div>
        <TransactionCreate />
        <TransactionHistoryList />
    </div>
)
export default App