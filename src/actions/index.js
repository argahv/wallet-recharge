
export const addTransaction = ({
    balance=1000,
    number='',
    amount=0
} = {}) => ({
        type:'ADD_TRANSACTION',
        wallet:{
            balance,
            number,
            amount
            
        }
    })